<?php

namespace App\Http\Controllers;

use App\Company;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        try {
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            die("Could not connect to the database. Please check your configuration. error:" . $e );
        }
    }

    public function userRegister(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'required',
            'password' => 'required|min:6'
        ]);

        try {
            $user = new User();

            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');
            $user->email = $request->get('email');
            $user->phone = $request->get('phone');
            $user->password = app('hash')->make($request->get('password'));

            $user->save();

            return response()->json(['status' => 'ok']);
        } catch (\Exception $e) {
            return response()->json(['status' => 'fail']);
        }
    }

    public function signIn(Request $request)
    {
        $this->validate($request, [
            'email' => 'email',
            'password' => 'password',
        ]);

        if (Auth::attempt($request->only('email', 'password'))) {
            return response()->json(['status' => 'ok']);
        } else {
            return response()->json(['status' => 'fail']);
        }
    }

    // todo signOut

    public function recoverPassword()
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users',
        ]);

       //todo send email
    }

    public function companies() {
        return response()->json(Company::where('user_id', Auth::id())->with('user')->get());
    }
}
