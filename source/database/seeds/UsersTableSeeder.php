<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrator = new User();
        $administrator->first_name = 'Sergey';
        $administrator->last_name = 'Scherbina';
        $administrator->email = 'azdeus@gmail.com';
        $administrator->password = app('hash')->make('secret');
        $administrator->phone = '+380965369562';
        $administrator->save();
    }
}
