<?php

use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ApiTest extends TestCase
{

    public function testDBConnection()
    {
        try {
            DB::connection()->getPdo();
            $this->assertTrue(true);
        } catch (\Exception $e) {
            $this->assertTrue(false);
        }
    }

    public function testRegistration()
    {
        // todo
    }

    public function testSignIn()
    {
        // todo
    }

    public function testRecoverPassword()
    {
        // todo
    }

    public function testCompanies()
    {
        // todo
    }
}
