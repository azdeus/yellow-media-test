# Test YellowMedia Technical exercise for PHP dev

Coworker: Sergey Scherbina

# Docker-контейнер

* Nginx (latest)
* PostgreSQL (latest)
* PHP (7.2)
* Lumen (latest)

## Установка

Клонируем репозиторий:
```
➜ cd ~
➜ git clone https://github.lab/azdeus/yellow-media-php-test
```

Устанавливаем зависимости:
```
➜ cd ~/yellow-media-php-test
➜ docker run --rm -v $(pwd)/source:/app composer install --ignore-platform-reqs
```

Определяем переменные окружения:
```
➜ cp source/.env.example source/.env
```

В `source/.env` необходимо указать правильные значения переменных для успешного подключения к базе данных.

```
DB_CONNECTION=pgsql
DB_HOST=db
DB_PORT=5432
DB_DATABASE=lumen
DB_USERNAME=postgres
DB_PASSWORD=secret
```

Запускаем контейнер
```
➜ docker-compose up -d
```

Если всё выполнилось успешно, то на http://localhost нас ждёт следующая ошибка:

```
SQLSTATE[42P01]: Undefined table: 7 ERROR: relation "users" does not exist LINE 1: SELECT * FROM users ^ (SQL: SELECT * FROM users)
```

Как видно из сообщения в базе нет таблицы `users`. Давайте добавим:

```
➜ docker-compose exec app php artisan migrate


```

Так же заполним таблицу записями:

```
➜ docker-compose exec app php artisan db:seed

Seeding: UsersTableSeeder
Seeded:  UsersTableSeeder (0.28 seconds)
Database seeding completed successfully.
```

Снова перейдём на http://localhost и теперь увидим в ответе массив записей (пользователей) из таблицы `users`.


