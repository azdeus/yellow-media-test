<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'Test YellowMedia Technical exercise for PHP dev of Sergey Scherbina';
});

$router->group(['prefix' => 'api/user'], function () use ($router) {
    $router->post('register', 'ApiController@userRegister');

    $router->post('sign-in', 'ApiController@signIn');

    $router->post('recover-password', 'ApiController@recoverPassword');
    $router->patch('recover-password', 'ApiController@recoverPassword');

    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->get('companies', 'ApiController@companies');
        $router->post('companies', 'ApiController@companies');
    });
});
